//Permet de faire du mediaquerie en JS lors de l'ouverture de la page mais aussi lors d'un changement de la largeur du viewport.
/*
Pour le changement on utilise un ecouteur et pour l'ini une fonction basique
Code Utilisé pour Maajesty
*/

        function remover() {
                    document.querySelector('.xCLaChild').classList.remove('limiter');
                    document.querySelector('.xCL').classList.remove('containerLimiter');
                    console.log("inf a to 1397 removing......")
                }

                function adder() {
                    document.querySelector('.xCLaChild').classList.add('limiter');
                    document.querySelector('.xCL').classList.add('containerLimiter');
                    console.log("sup to 1397px adding........")
                }
                var a = 1;

                function t() {
                    a = a + 1;
                    return a;
                    console.log(a)
                }
                for (i = 0; i < 3; i++) {
                    t("a");
                }
                var ui = t("a"); // lors de l'appel de ui et a ces derniers valent 5
                //mais si on refaits un test('a'); puis on l'appele on aura 6
                //et si on reappel ui on a tjrs 5 car la fonctiona été exécuté une
                var value = 1397
                var mql = window.matchMedia(`(max-width: ${value}px)`);

                function mediaqueryresponse(args) {
                    if (args == "mq") {
                        if (mql.matches) {
                            remover();
                        } else {
                            adder();
                        }
                        console.warn("Initialized only once time")
                    }
                }
                //Fonctions qui va s'éxécuter dès l'actualisation de la page mais pas au changement de resizer
                mediaqueryresponse("mq")
                //On ajoute une fonction ecouteur qui prendra en argument une fonction callback qui va s'exécuter au resize de la page par niveau de la corespondance
                mql.addListener(() => {
                    mediaqueryresponse('mq')
                });


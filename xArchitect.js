// @by Ashwin Malbrouck ak AMK

function xArchitect(targets,args)
{
  this.targets = targets;
  this.args = args;
  
}


xArchitect.prototype.setBasicProperty = function(targets,args)
{
  var targets = document.querySelectorAll('.'+targets);

  var elementsNumber = targets.length;
  for(var i=0;i<elementsNumber;i++)
  {
    targets[i].style.height=            args.heightElements + "px";
    targets[i].style.width =           args.widthElements + "px";
    targets[i].style.borderColor =     args.bordercolorElements
    targets[i].style.backgroundColor = args.backgroundColorElements;
    targets[i].style.borderWidth =     args.borderWidthElements + " px ";
    targets[i].style.transition =      args.transitionElements+"s";
    targets[i].style.boxShadow =       args.boxShadowElements;
    targets[i].style.borderWidth =     args.borderWidthElements;
  }




}

xArchitect.prototype.xObserver = function(){}






//Exemple d'appele sur une classe "box" et "box2"
 var architect = new xArchitect();

        architect.setBasicProperty('box',
        {
          boxShadowElements:"-1px 1px 2px black",
          transitionElements:" all 2s",
          heightElements:"400",
          widthElements:'400',
          
        })


       

        architect.setBasicProperty('box2',
        {
          transitionElements:"3s",
          boxShadowElements:"-1px 1px 20px red",
          transitionElements:"2",
          heightElements:"400",
          widthElements:'900',
        
          
        })


xArchitect.prototype.advancedProperty = function(){}